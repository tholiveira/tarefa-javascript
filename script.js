const buttonForm = document.querySelector("#add-book");
const elementList = document.querySelector("#father-list");
const inputForm = document.querySelector("#add-book input");

buttonForm.addEventListener("click",(e)=>{
    e.preventDefault();
    if(e.target.nodeName == "BUTTON"){
        let newLi = document.createElement('li');
        let newSpanName = document.createElement('span');
        let newSpanDelte = document.createElement('span');
        newSpanName.classList.add("name");
        newSpanDelte.classList.add("delete");
        newSpanDelte.innerText = "excluir";
        newSpanName.innerText = inputForm.value;    
        newLi.appendChild(newSpanName);
        newLi.appendChild(newSpanDelte);
        elementList.appendChild(newLi);
    } 
});

elementList.addEventListener("click",(e) => {
    if(e.target.nodeName == "SPAN" && e.target.className == "delete")   e.target.parentNode.remove();
});